import loginRoutes from "./loginRoutes";
import gameRoutes from "./gameRoutes";
import joinGameRouter from "./joinGameRoutes"

export default app => {
  app.use("/login", loginRoutes);
  app.use("/game", gameRoutes);
  app.use("/join-to-game", joinGameRouter);

};
