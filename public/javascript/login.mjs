const username = sessionStorage.getItem("username");

if (username) {
  window.location.replace("/join-to-game");
}

const submitButton = document.getElementById("submit-button");
const input = document.getElementById("username-input");

const getInputValue = () => input.value;

const users = [];

const addUserSocket = (inputValue) => {
  socket.emit("add user", inputValue)
}

const socket = io("http://localhost:3003/login");

socket.on("all users", users => {
  console.log(users)
})

const onClickSubmitButton = () => {
  const inputValue = getInputValue();
  if (!inputValue) {
    return;
  }
  sessionStorage.setItem("username", inputValue);
  users.push(inputValue);
  addUserSocket(inputValue)
  window.location.replace("/join-to-game");
};

const onKeyUp = ev => {
  const enterKeyCode = 13;
  if (ev.keyCode === enterKeyCode) {
    submitButton.click();
  }
};

submitButton.addEventListener("click", onClickSubmitButton);
window.addEventListener("keyup", onKeyUp);
