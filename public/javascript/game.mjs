const username = sessionStorage.getItem("username");

if (!username) {
    window.location.replace("/login");
}

const socket = io("http://localhost:3000/game", { query: { username } });

let allUsers = [];
allUsers.push(username)

const usersDiv = document.getElementById("users");
const allUsersDiv = document.createElement("div");
usersDiv.append(allUsersDiv)

const setReady = document.getElementById('btn-status');
let isReady = false;

const addUser = (className, user, status)=> {
    let newUser = document.createElement("div");
    newUser.innerText = user.username;
    newUser.classList.toggle(className);
    newUser.setAttribute("id", user.username)
    allUsersDiv.append(newUser)
    // setReady.innerHTML = status

    return newUser
}

socket.on("add user", users => {
    allUsersDiv.innerHTML = "";

    users.forEach( user => {
        if (user.username === username) {
            addUser("bg-red", user, "Ready").innerHTML = username + " (you)";
        } else {
            if (user.isReady) {
                addUser("bg-green", user, "Not ready")
            } else {
                addUser("bg-red", user, "Ready")
            }
        }
    })
})

socket.on("disconnect", user => {
    const elem = document.getElementById(user);
    elem.remove()
})

const setStatus = (user, cl1, cl2, status) => {
    const elem = document.getElementById(user.username);
    if (elem.className === cl1) {
        return
    }
    setReady.innerHTML = status
    elem.classList.remove(cl2)
    elem.classList.toggle(cl1)
}

socket.on("setReady", users => {
    users.forEach( user => {
        if(user.username) {
            if(user.isReady){
                setStatus(user, "bg-green", "bg-red", "Not ready");
            } else {
                setStatus(user, "bg-red", "bg-green", "Ready");
            }
        }
    })
})

let counter = 0;
let counterDiv = document.getElementById('timer');
const setBtn = () => {
    socket.emit("isReady", username)
    socket.on("counter", (users, SECONDS_TIMER_BEFORE_START_GAME) => {
        users.forEach(user => {
            if (user.isReady) {
                counter++
            }
        })
        if (counter === users.length && counter >= 2) {

            socket.emit("send counter")
        }
    })
}

socket.on("start counter", counter => {
    setReady.classList.toggle('none');
    counterDiv.classList.toggle('block');
    counterDiv.innerText = counter.toString()
    setTimeout(() => socket.emit("get new order"),1000 )
})

socket.on("new order")


setReady.addEventListener("click", setBtn)