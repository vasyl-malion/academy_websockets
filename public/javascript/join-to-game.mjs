const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const joinBtn = document.getElementById("join")

const joinGameBtn = () => {
  window.location.replace("/game");
};

joinBtn.addEventListener("click", joinGameBtn);
