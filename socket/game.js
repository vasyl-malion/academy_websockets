let users = [];
import SECONDS_TIMER_BEFORE_START_GAME from "./config";

export default io => {
    io.on("connection", socket => {

        const username = socket.handshake.query.username;
        users.push({username, isReady: false})

        socket.on("disconnect", () => {
            let thisUser = users.findIndex(user => user.username === username)
            users.splice(thisUser, 1);
        })

        io.emit("add user", users)

        socket.on("isReady", username => {
            const ind = users.findIndex(user => user.username === username)
            users[ind].isReady = !users[ind].isReady
            io.emit("setReady", users)

            io.emit("counter", users)

        })

        let startCounter = SECONDS_TIMER_BEFORE_START_GAME;
let timer = 10;
        socket.on("send counter", () => {
            io.emit("start counter", timer)
        })

        socket.on("get new order", () => {
            io.emit("new counter", --timer)
        })
    });
};

